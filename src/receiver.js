'use strict';

 exports.handler = function(event, context, callback) {
    var data = {'a': 'b'};

    var response = {
        statusCode: 200,
        headers: {
            "x-generator" : "Glue Code Workshop"
        },
        body: JSON.stringify(data)
    };

    callback(null, response);
 };
