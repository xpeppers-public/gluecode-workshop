# Introduction

This README will work you through the entire workshop and the creation of all resources needed. 
Purpose of this workshop is to show you how to build a glue application using Serveless architecture
model.

As glue application we mean, first of all an application able to merge two different worlds together
(e.g. mobile and cloud). But we mean, also an application that seamlessly integrates informations from different
sources and services. 

This two goals should be attained writing as less code as possible, leveraging the facilities provided by
Amazon Web Services like AWS Lambda and other tools.

## What's Here

This sample includes:

* README.md - this file
* buildspec.yml - this file is used by AWS CodeBuild to package your
  application deployment
* template.yml - this file contains the AWS Serverless Application Model (AWS SAM) used
  by AWS CloudFormation to deploy your application to AWS Lambda and Amazon API
  Gateway. Furthermore this template file contains information related to AWS DynamoDB setup, 
  along with Step function definition.
* src/ - Lambda related stuff

## Prerequisities

* A basic understanding of Javascript and NodeJS;
* A Git client and minimal Git knowledge;
* Your preferred IDE (Atom, Sublime, VS Code,...);
* An AWS account and basic understanding of cloud models;

## What you will do?
During the workshop you will see how to:

* Use Lambda with API Gateway to create ReST API;
* Use DynamoDB to give your serverless application a realiable NoSQL database;
* Leverage Step Function to handle your workflow;
* Extend your application sending and receiving SMS using external service;
* ...and much more

# Step by step guide
These are the steps needed, in order, to get our serverless application up and running. Are you ready?

## Step 1 - Start a CodeStar project and Git repository setup
* Choose a NodeJS WebApplication with Lambda backend when creating new CodeStar project. 
* Clone the repository and replace the old repository with this one;
```
git clone git@gitlab.com:xpeppers-public/gluecode-workshop.git
```

## Step 2 - Implement Receiver Lambda
* (starting from HelloWorld) Define Lambda/Events Receiver in template (show Lambda/APIGW console, Test Lambda);
* Implement Lambda response so ApiGW is happy
* Verify APIGW correctly returns a mocked JSON

## Step 2 bis - Configure Twilio
* Configure Twilio (fast recap how Twilio Works: it needs a callback);
* Use our Receiver API as callback;
* Understand TwiML

## Step 3 - Extend Receiver Lambda
* Edit callback so it can return TwiML (npm install twilio here);
* Make a call to Receiver Lambda using Twilio;
* Print the event variable returned by Twilio;

## Step 4 - Create a Lambda send-notification
* Define Lambda send-notification in template (no APIGW needed here)
* Code Lambda so we can ask Twilio to send an SMS
* test the Lambda;

## Step 5 - Implement Step functions
* Define a Step Function with a single state: send-notification;
* Define the SF's role;
* Start manually the step function and check if it sends the SMS

## Step 6 - Bind Receiver with Step function
* Install querystring and uuid4 modules
* Prepare Step Function input;
* Enhance Receiver Lambda so it can start execution
* Hit Receiver Lambda HTTPEndpoint and wait for message to returns (this close the loop)

## Step 7 - Setup Companies database
* Create the database from template
* Insert some data from console

## Step 8 - Setup arrange Lambda
* Define Arange Lambda in template.yml;
* Use DyanmoDB to read from DB and show content (no CustomError here)

## Step 9 - Put Arrange Lambda in StepFucntion
* Edit SF definition to use Arrange as first task
* Show how output of a Lambda is the input for the next Lambda (console)

## Step 10 - Handle workflow error (company not found)
* Show how to raise an error in Arrange Lambda
* Configure Catch in Step Function definition
* Handle both successfull and failure notification in send-notification Lambda

## Step 11 - Implement Parallel State
* Implement parallel in StepFunction definition between arrange and send-notification state (notice how output of state is sent to each parallel task):

## Step 12 - Implement Glue State
* Here you can merge differente information together 
* Just conceptual nothing really intersting in JSON editing

## Step 13 - Setup Reports database
* Create Database in cloudformation
* Implement a new state for saving report
* Use execution_id as report_id to correlate operations

## Setup 14 - Setup GetReportById API
* Implement API GW with Path (show path parameters)
* Implement LAmbda
* Calling a report